// Напишите код, который сделает из массива объект
// На входе массив
var arr = [
 {name: 'width', value: 10}, 
 {name: 'height', value: 20}
];

// На выходе объект {width: 10, height: 20}
let obj={};

arr.forEach((each)=>{
    obj[each.name]=each.value
})

// 2
// Что выведет в консоль следующий код?
Promise.resolve(1)
  .then(x => x + 1)
  .then(x => { throw x })
  .then(x => console.log(x))
  .catch(err => console.log(err)) 
  .then(x => Promise.resolve(1))
  .catch(err => console.log(err))
  .then(x => console.log(x))

2
1

// 3
// Напишите код, который при клике на любой  div внутри  root будет выводить в консоль его  id. 
// Можно использовать jQueryj
<div id="root" style="background: red;">
    root
    <span id="id1" style="background: lightblue;">id1</span>
    <div id="id2" style="background: green;">
        id2
        <div id="id3" style="background: yellow;">id3</div>
    </div>
        <div id="id6" style="background: yellow;">id3</div>
        <div id="id9" style="background: yellow;">id3</div>
</div>

$('#root *').click(function(){
    let elemId = $(this).attr('id')
    console.log(elemId)
})

// Как сделать, чтобы работало в случае, когда верстка динамически изменяется внутри элемента root
// Что такое делегирование события
// Какие бывают фазы события

// 4
<div style="float: left; border: 1px solid red;">
    <span style="position: relative; top: -1000px; left: -1000px; border: 1px solid green;">
        Inner text
    </span>
</div>

// 5
/**
 * Есть функция и объект. Необходимо, чтобы функция в консоли вывела значение 'yandex'.
 * Как добиться желаемого, не изменяя тело функции?
 */
function f() { console.log(this.x); }
var obj = {x: 'yandex', func:f };

obj.f()

f.call(obj)

// 6

// Написать функцию, которая отсортирует массив таким образом, чтобы все нечетные по значению числа в этом массиве оказались отсортированы по возрастанию, а все четные оставить на своих местах.
// Например, для массива
[ 2, 3, 7, 4, 6, 1, 5, 8, 9 ]
// получим
[ 2, 1, 3, 4, 6, 5, 7, 8, 9 ]

let arr1 = [ 2, 3, 7, 4, 6, 1, 5, 8, 9 ]
let arr2 = []

arr1.forEach((each,index)=>{
    
    if(each%2==1){
        arr1[index]=0;
        arr2.push(arr1[index])
    }
    
})

arr2.sort((a, b) => Math.sign(a - b))

let flag = 0;
arr1.forEach((each,index)=>{
    
    if(each===0){
        arr1[index]=arr2[flag]
        flag+=1;
    }
    
})

