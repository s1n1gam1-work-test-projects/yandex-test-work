/**
 * Банкомат.
 *
 * В банкомате есть купюры - 50, 100, 500, 1000, 5000 руб.
 * Есть ограничение на количество каждой из купюр (limits), его нужно держать в актуальном состоянии
 * Нужно вернуть купюры и их количество, которыми можно выдать запрашиваемую сумму,
 * в виде строки указанного формата. Начинать с самой крупной.
 * Если выдать запрашиваемую сумму не получается, вернуть ошибку.
*/

const limits = [
        {
            paper:5000,
            limit:200
        },
        {
            paper:500,
            limit:200
        },
        {
            paper:200,
            limit:500
        },
        {
            paper:50,
            limit:200
        },
    ]; // нужно выбрать любой удобный формат

atm(1250, limits) // '1x1000 2x100 1x50'
atm(1000000, limits) // 'Error' // Not enough money
atm(2400, limits) // '2x1000 4x100'
atm(6512, limits) // 'Error' // Incorrect value

function atm(sum, limits) {
    
    let result = "";
    
    limits.forEach((each,index)=>{
        
        if(sum % each.paper == 0 ){
            result+=each+"x"+sum/each
            
        } else if(each.paper < sum) {
            
            let count = (sum - sum % each.paper ) / each.paper;
            sum-= each.paper * count;
            
            result+= each.paper+"x"+count+" ";
            
        }
        
    })
    
    
    
}